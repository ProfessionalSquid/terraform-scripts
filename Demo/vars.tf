variable "location" {
  type    = string
  #Change location to datacenter nearest to host
  default = "eastus"
}
variable "prefix" {
  type    = string
  #Change this variable in order to change name of all VMs
  default = "demo"
}

variable "ssh-source-address" {
  type    = string
  default = "*"
}
