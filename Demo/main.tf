provider "azurerm" {
  version = "=1.35.0"
}

# Create a resource group
resource "azurerm_resource_group" "demo" {
  # Change name of resource group here
  name     = "Test-Group-Arsiem"
  location = var.location
}
