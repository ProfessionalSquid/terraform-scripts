# 1 VM, 1 Public IP, 1 Disk, 1 Disk Attachment

# Data template Bash bootstrapping file
data "template_file" "linux-vm-cloud-init" {
  template = file("azure-install-rdp.sh")
}
resource "azurerm_virtual_machine" "moodle-vm" {
  name                  = "Moodle-vm"
  location              = var.location
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.moodle-instance.id]
  vm_size               = "Standard_A1_v2"

  delete_os_disk_on_termination = true
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
  storage_os_disk {
    name = "myosdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "moodle-instance"
    admin_username = "arsiem"
    admin_password = "Arsiem2020!!"
    custom_data = base64encode(data.template_file.linux-vm-cloud-init.rendered)
  }
  os_profile_linux_config {
      #when an Admin password is specified, this variable must be set to false.
    disable_password_authentication = false
    ssh_keys {
      # Note: If you did ssh-keygen and used a different name, replace mykey.pub with the new public key name
      # Additionally, make sure to change the name of the path parameter to account for admin username (if you change from moodleAdmin)
      key_data = file("mykey.pub")
      path     = "/home/arsiem/.ssh/authorized_keys"
    }
  }
  tags = {
    environment = "Moodle Dev"
  }
}

resource "azurerm_managed_disk" "moodle-disk" {
  name                 = "Moodle-Disk"
  location             = var.location
  resource_group_name  = azurerm_resource_group.rg.name
  storage_account_type = "Standard_LRS"
  #Change this to import, and then for source_uri, add the path to the VHD to import.
  create_option        = "Empty"
  disk_size_gb         = "30"

  tags = {
    environment = "Moodle Dev"
  }
}

resource "azurerm_public_ip" "moodle-instance" {
    name                         = "moodle-instance1-public-ip"
    location                     = var.location
    resource_group_name          = azurerm_resource_group.rg.name
    allocation_method            = "Static"
}
