# 1 NSG, 1 NIC, Vnet, Storage Acc, Recovery Services Vault, DNS zone

resource "azurerm_network_security_group" "moodle-instance" {
  name                = "moodle-instance-nsg"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name


  security_rule {
    name                       = "Allow-SSH"
    description                = "Allow SSH"
    priority                   = 300
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Allow-RDP"
    description                = "Allow RDP"
    priority                   = 299
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "3389"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
   security_rule {
    name                       = "allow-http"
    description                = "allow-http"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "Internet"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_interface" "moodle-instance" {
  name                      = "moodle-instance1"
  location                  = var.location
  resource_group_name       = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "instance1"
    private_ip_address_allocation = "Dynamic"
    subnet_id                     = azurerm_subnet.moodle-instance.id
    public_ip_address_id          = azurerm_public_ip.moodle-instance.id
  }
}

resource "azurerm_virtual_network" "vnet1" {
  name                = "vnet1-moodle"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  address_space       = ["10.0.18.0/24"]

}
resource "azurerm_subnet" "moodle-instance" {
  name                 = "moodle-subnet"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet1.name
  address_prefixes     = ["10.0.18.0/24"]
}


resource "azurerm_storage_account" "storageacc" {
  name                     = "storageacc109u532"
  resource_group_name      = azurerm_resource_group.rg.name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_network_interface_security_group_association" "moodle-instance" {
  network_interface_id      = azurerm_network_interface.moodle-instance.id
  network_security_group_id = azurerm_network_security_group.moodle-instance.id
}