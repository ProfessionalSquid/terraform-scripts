#Resources: DNS zone, VM, Public IP, NSG, Disk, VNet, NIC, Storage Acc, Recovery Services Vault

provider "azurerm" {
    version="2.41.0"
    features{}
}

# create resource group (change the name for new instances)
resource "azurerm_resource_group" "rg"{
    name = "MoodleDev-rg"
    location = var.location
}

