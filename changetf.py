# This file takes input from a flask app, and modifies the Terraform main.tf files in both the NMapLab and PacketLab resource groups,
#  and deletes the terraform.tfstate files located in those folders in order to spool up new instances of resource groups
import os
import time
import subprocess


numStudents = int(input("Enter number of students: "))
resourceGroupNum = 1
for x in range(numStudents):

    #############################################################################
    #                    CHANGE THIS PATH TO YOUR PACKETLAB 'main.tf' DIRECTORY #
    #############################################################################
    with open(r'C:\Users\Sean\Desktop\Code Projects\terraform-scripts\PacketLab\main.tf', 'r+') as f: #r+ does the work of rw
        lines = f.readlines()
        
        for i, line in enumerate(lines):

            if line.strip().startswith('name'):
           
                x = line.split('"')
                if int(resourceGroupNum) == 1:
                    x[1] += str(resourceGroupNum)
                else:
                    newx = x[1].replace(str(int(resourceGroupNum - 1)), str(resourceGroupNum))
                    x[1] = newx
                resourceGroupNum = int(resourceGroupNum) + 1
                
                newline = '"'.join(x)
               
                lines[i] = newline

        f.seek(0)
        for line in lines:
            f.write(line)
    f.close()
    #############################################################################
    #                    CHANGE THIS PATH TO YOUR PACKETLAB DIRECTORY           #
    #############################################################################
    os.chdir(r"C:\Users\Sean\Desktop\Code Projects\terraform-scripts\PacketLab")
    os.system("terraform apply -auto-approve")
    os.system("del /f terraform.tfstate")
    os.system("del /f terraform.tfstate.backup")
    resourceGroupNum = 1

    #############################################################################
    #                    CHANGE THIS PATH TO YOUR MOODLEDEV 'main.tf' DIRECTORY #
    #############################################################################
    with open(r'C:\Users\Sean\Desktop\Code Projects\terraform-scripts\MoodleDev\main.tf', 'r+') as f: #r+ does the work of rw
        lines = f.readlines()
        
        for i, line in enumerate(lines):

            if line.strip().startswith('name'):
           
                x = line.split('"')
                if int(resourceGroupNum) == 1:
                    x[1] += str(resourceGroupNum)
                else:
                    newx = x[1].replace(str(int(resourceGroupNum - 1)), str(resourceGroupNum))
                    x[1] = newx
                resourceGroupNum = int(resourceGroupNum) + 1
                
                newline = '"'.join(x)
               
                lines[i] = newline

        f.seek(0)
        for line in lines:
            f.write(line)
    f.close()
    #############################################################################
    #                    CHANGE THIS PATH TO YOUR MOODLEDEV DIRECTORY           #
    #############################################################################
    os.chdir(r"C:\Users\Sean\Desktop\Code Projects\terraform-scripts\MoodleDev")
    os.system("terraform apply -auto-approve")
    os.system("del /f terraform.tfstate")
    os.system("del /f terraform.tfstate.backup")